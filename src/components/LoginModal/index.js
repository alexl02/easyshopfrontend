import axios from 'axios';
import { useState } from 'react';
import { httpPost } from '../../http';
import LoginModalView from "./LoginModalView";

import { connect } from 'react-redux';
import { getCustomerSessionAction } from '../../redux/actions/CustomerSessionActions';

const LoginModal = ({ showModal, onShowLoginModal, onShowSignupModal, getCustomerSessionAction }) => {

    const [ form, setForm ] = useState({
        email: '',
        password: '',
        error: {
            email: false,
            password: false
        },
        msg: {
            email: '',
            password: ''
        },
        loading: false
    });
    
    const handleSignupClick = () => {
        onShowLoginModal();
        onShowSignupModal();
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setForm({
            ...form,
            loading: true
        });
        let error = false;
        let msg = "";
        let newForm = {
            ...form,
            error: {
                email: false,
                password: false
            },
            msg: {
                email: '',
                password: ''
            },
            loading: false
        }
        if(form.email.length < 1){
            error = true;
            msg = msg + "Debe ingresar un correo electronico.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    email: true
                },
                msg: {
                    ...newForm.msg,
                    email: "Debe ingresar un correo electronico."
                }
            };
        }

        if(form.password.length < 1){
            error = true;
            msg = msg + "Debe ingresar una contraseña.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    password: true
                },
                msg: {
                    ...newForm.msg,
                    password: "Debe ingresar una contraseña."
                }
            };
        }

        if(!error) {
            let data = {email: form.email, password: form.password};
            try{
                const resp = await httpPost("/customer/login", data);
                console.log(resp);
                if(resp.data){
                    localStorage.setItem("sessionId", resp.data.id);
                    getCustomerSessionAction(resp.data.id);
                }
                setForm({
                    email: '',
                    password: '',
                    error: {
                        email: false,
                        password: false
                    },
                    msg: {
                        email: '',
                        password: ''
                    }
                });
                onShowLoginModal();
            } catch (err) {
                console.log(err);
                if (err instanceof axios.AxiosError){
                    if((err.response.data.code) && (err.response.data.code === 'ERROR_DATA_NOT_FOUND')){
                        error = true;
                        msg = msg + "Correo o contraseña incorrectos.\n\n";
                        newForm = {
                            ...newForm,
                            error: {
                                ...newForm.error,
                                email: true,
                                password: true
                            },
                            msg: {
                                ...newForm.msg,
                                email: "Correo o contraseña incorrectos.",
                                password: "Correo o contraseña incorrectos."
                            }
                        };
                    }
                }
            }
        } else {
            console.log(msg);
        }

        setForm(newForm);
    }

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const handleClose = () => {
        setForm({
            email: '',
            password: '',
            error: {
                email: false,
                password: false
            },
            msg: {
                email: '',
                password: ''
            }
        });
        onShowLoginModal();
    }
    
    return(
        <LoginModalView showModal={showModal} onShowModal={onShowLoginModal} onSignupClick={handleSignupClick} onSubmit={handleSubmit} form={form} onFormChange={handleChange} onClose={handleClose} />
    )
}

const mapStateToProps = ({ }) => {
    return {
        
    };
};

const mapDispatchToProps = {
    getCustomerSessionAction
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);