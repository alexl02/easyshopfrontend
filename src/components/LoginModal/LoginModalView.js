import Modal from '../Modal';
import CustomLink from '../CustomLink';
import { LoadingOutlined } from '@ant-design/icons';
import Input from '../Input';
import './LoginModal.css';

const LoginModalView = ({ showModal, onSignupClick, form, onFormChange, onClose, onSubmit }) => {
    return(
        <Modal
            showModal={showModal}
            onShowModal={onClose}
            title="Inicia sesión"
        >
            <div className='login-modal-container'>
                <form className="login-modal-form" onSubmit={onSubmit}>
                    <div className="form-field">
                        <Input type="email" name="email" label="Correo electronico:" value={form.email} onChange={onFormChange} error={form.error.email} msg={form.msg.email} />
                    </div>
                    <div className="form-field">
                        <Input type="password" name="password" label="Contraseña:" value={form.password} onChange={onFormChange} error={form.error.password} msg={form.msg.password} />
                    </div>
                    { form.loading ?
                        <div className="login-modal-from-spinner"><LoadingOutlined /></div>
                        :
                        <div className="login-modal-form-buttons-container">
                            <input type="submit" className="custom-button" value="Aceptar" />
                        </div>
                    }
                </form>
                <br />
                <br />
                <div><CustomLink><div className="login-modal-link">¿Olvidaste tu contraseña?</div></CustomLink></div>
                <br />
                <div><CustomLink onClick={onSignupClick}><div className="login-modal-link">¿Aún no tienes una cuenta? Registrate aqui</div></CustomLink></div>
            </div>
        </Modal>
    );
}

export default LoginModalView;