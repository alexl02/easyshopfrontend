import InputView from "./InputView";

const Input = ({ type, label, error, msg, name, value, onChange }) => {
    return(<InputView type={type} label={label} error={error} msg={msg} name={name} value={value} onChange={onChange} />);
}

export default Input;