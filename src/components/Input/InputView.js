import './Input.css';

const InputView = ({ type, label, error, msg, name, value, onChange }) => {
    return(
        <div className="input-main-container">
            <div className="input-container">
                <label className="input-label">{label}</label>
                <input type={type} className={ !error ? "input" : "input-error"} name={name} value={value} onChange={onChange} />
            </div>
            <div className="input-msg">{msg}</div>
        </div>
    );
}

export default InputView;