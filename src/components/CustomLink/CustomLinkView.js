import './CustomLink.css';

const CustomLinkView = ({children, onClick}) => {
    return(
        <button className="custom-link-button" onClick={onClick}>{children}</button>
    );
}

export default CustomLinkView;