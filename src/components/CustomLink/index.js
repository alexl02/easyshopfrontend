import { Children } from "react";
import CustomLinkView from "./CustomLinkView";

const CustomLink = ({children, onClick}) => {
    return(
        <CustomLinkView onClick={onClick}>{children}</CustomLinkView>
    );
}

export default CustomLink;