import ShoppingCartListView from "./ShoppingCartListView";

import { connect } from 'react-redux';
import { clearProductsAction } from '../../redux/actions/ShoppingCartActions';

const ShoppingCartList = (props) => {
    
    const { products, showShoppingCart, clearProductsAction, onOrder } = props;

    return(
        <ShoppingCartListView 
            products={products}
            showShoppingCart={showShoppingCart}
            clearProductsAction={clearProductsAction}
            onOrder={onOrder}
        />
    );
}

const mapStateToProps = ({ ShoppingCartReducer }) => {
    return {
        
    };
};

const mapDispatchToProps = {
    clearProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartList);