import { useState, useEffect } from 'react';
import './ShoppingCartList.css';
import DefaultImg from '../../img/default.png';

import { connect } from 'react-redux';
import ShoppingCartReducer from "../../redux/reducers/ShoppingCartReducer";
import { setProductsAction } from "../../redux/actions/ShoppingCartActions";

const ShoppingCartListItem = (props) => {

    const { product, ShoppingCartReducer, setProductsAction } = props;
    
    const [ qty, setQty ] = useState(0);

    useEffect( () => {
        setQty(product.qty);
    }, []);

    useEffect( () => {
        const index = ShoppingCartReducer.products.findIndex( p => (p.id === product.id) && (p.price.um === product.price.um));
        if(index > -1) {
            setQty(ShoppingCartReducer.products[index].qty);
        } else {
            setQty(0);
        }
    }, [ShoppingCartReducer.products] )

    const handlePlus = () => {
        let p = { ...product }
        p.qty = qty + 1;
        setProductsAction(p);
        setQty(qty + 1);
    }

    const handleMinus = () => {
        if(qty > 0){
            let p = { ...product }
            p.qty = qty - 1;
            setProductsAction(p);
            setQty(qty - 1);
        }
    }

    const handleChange = (e) => {
        setQty(e.target.value);
    }

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    return(
        <div className="shoppingcart-item-container">
            <div className="shoppingcart-item">
                <div className="shoppingcart-item-img-container"><img className="shoppingcart-item-img" src={product.imgUrl} onError={handleImgError}   /></div>
                <div className="shoppingcart-item-info">
                    <div className="shoppingcart-item-info-name">{product.name}</div>
                    <div className="shoppingcart-item-info-price">Precio: ${ product.price.price.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                    <div className="shoppingcart-item-info-sku">PLU: {product.erpProduct.sku}</div>
                    
                </div>
                
            </div>
            <div className="shoppingcart-item-footer">
                <div className="shoppingcart-item-info-total">Valor total: ${ (product.price.price * product.qty).toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                <div className="shoppingcart-list-qty-container">
                    <button className="shoppingcart-list-qty-minus" onClick={handleMinus}>-</button>
                    <input type="numeric" className="shoppingcart-list-qty-input" value={qty} onChange={handleChange} />
                    <button className="shoppingcart-list-qty-plus" onClick={handlePlus}>+</button>
                </div>
            </div>
        </div>
    );
}

const ShoppingCartListView = (props) => {

    const { products, ShoppingCartReducer, setProductsAction, showShoppingCart, clearProductsAction, onOrder } = props;

    return (
        <div className={ showShoppingCart ? "shoppingcart-list-container-active" : "shoppingcart-list-container" }>
            <div className="shoppingcart-list-title-container">
                <div className="shoppingcart-list-title">Tu pedido</div>
                <button className="custom-button" onClick={clearProductsAction}>Limpiar carrito</button>
            </div>
            <div className="shoppingcart-items">
                {
                    products.map( (product, index) => {
                        return(
                            <ShoppingCartListItem key={index} product={product} setProductsAction={setProductsAction} ShoppingCartReducer={ShoppingCartReducer}  />
                        );
                    } )
                }
                <div style={{ width: '100%', height: '400px' }}></div>
            </div>
            <div className="shoppingcart-items-total-container">
                <div className="shoppingcart-list-total shoppingcart-list-title">Total de pedido: ${ ShoppingCartReducer.total.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                <button className="custom-button" onClick={onOrder}>Hacer pedido</button>
            </div>
        </div>
    );
}

const mapStateToProps = ({ ShoppingCartReducer }) => {
    return {
        ShoppingCartReducer
    };
};

const mapDispatchToProps = {
    setProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCartListView);