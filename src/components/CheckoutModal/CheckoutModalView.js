import Modal from '../Modal';
import SimpleInput from '../SimpleInput';
import './CheckoutModal.css';
import Pse from '../../img/boton-pse.png';
import CustomLink from '../CustomLink';

const BillIcon = () => {
    return(
        <svg className="checkout-modal-payment-method-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M400 256C400 317.9 349.9 368 288 368C226.1 368 176 317.9 176 256C176 194.1 226.1 144 288 144C349.9 144 400 194.1 400 256zM272 224V288H264C255.2 288 248 295.2 248 304C248 312.8 255.2 320 264 320H312C320.8 320 328 312.8 328 304C328 295.2 320.8 288 312 288H304V208C304 199.2 296.8 192 288 192H272C263.2 192 256 199.2 256 208C256 216.8 263.2 224 272 224zM0 128C0 92.65 28.65 64 64 64H512C547.3 64 576 92.65 576 128V384C576 419.3 547.3 448 512 448H64C28.65 448 0 419.3 0 384V128zM48 176V336C83.35 336 112 364.7 112 400H464C464 364.7 492.7 336 528 336V176C492.7 176 464 147.3 464 112H112C112 147.3 83.35 176 48 176z"/></svg>
    );
}

const CreditCardIcon = () => {
    return(
        <svg className="checkout-modal-payment-method-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M168 336C181.3 336 192 346.7 192 360C192 373.3 181.3 384 168 384H120C106.7 384 96 373.3 96 360C96 346.7 106.7 336 120 336H168zM360 336C373.3 336 384 346.7 384 360C384 373.3 373.3 384 360 384H248C234.7 384 224 373.3 224 360C224 346.7 234.7 336 248 336H360zM512 32C547.3 32 576 60.65 576 96V416C576 451.3 547.3 480 512 480H64C28.65 480 0 451.3 0 416V96C0 60.65 28.65 32 64 32H512zM512 80H64C55.16 80 48 87.16 48 96V128H528V96C528 87.16 520.8 80 512 80zM528 224H48V416C48 424.8 55.16 432 64 432H512C520.8 432 528 424.8 528 416V224z"/></svg>
    );
}

const PaymentMethodOption = ({ id, Icon, label }) => {
    return(
        <div className='checkout-modal-payment-method-option'>
            <input type="radio" id={id} />
            <label htmlFor={id} >
                <div className="checkout-modal-payment-method-label">
                    <div className="checkout-modal-payment-method-icon-container"><Icon /></div>
                    <div className='checkout-modal-payment-method-label-text'>{label}</div>
                </div>
            </label>
        </div>
    );
}

const PaymentMethodOptionImg = ({ id, Img, label }) => {
    return(
        <div className='checkout-modal-payment-method-option'>
            <input type="radio" id={id} />
            <label htmlFor={id} >
                <div className="checkout-modal-payment-method-label">
                    <div className="checkout-modal-payment-method-icon-container"><img className='checkout-modal-payment-method-img' src={Img} alt="boton pse" /></div>
                    <div className='checkout-modal-payment-method-label-text'>{label}</div>
                </div>
            </label>
        </div>
    );
}

const AddressForm = ({ onNext }) => {
    return(
        <form className='checkout-modal-form' onSubmit={onNext}>
            <div className="checkout-modal-address-container">
                <select className="checkout-modal-select">
                    <option value="Calle">Calle</option>
                    <option value="Carrera">Carrera</option>
                    <option value="Diagonal">Diagonal</option>
                    <option value="Transversal">Transversal</option>
                    <option value="Callejon">Callejon</option>
                </select>
                <div className='checkout-modal-input-street-container'>
                    <SimpleInput type="text" name="name" />
                </div>
                <div className='checkout-modal-input-separator-char'>#</div>
                <div className='checkout-modal-input-street-container'>
                    <SimpleInput type="text" name="name" />
                </div>
                <div className='checkout-modal-input-separator-char'>-</div>
                <div className='checkout-modal-input-street-container'>
                    <SimpleInput type="text" name="name" />
                </div>
            </div>
            <div className="checkout-modal-address-details-container">
                <SimpleInput type="text" name="name" placeholder='Conjunto, Torre, Apto.' />
            </div>
            <div className="checkout-modal-address-tel-container">
                <SimpleInput type="text" name="name" placeholder='Numero telefonico de contacto.' />
            </div>
            <br />
            <br />
            <div className="checkout-modal-form-buttons-container">
                <input type="submit" className="custom-button" value="Siguiente" />
            </div>
        </form>
    );
}

const PaymentMethodForm = ({onNext}) => {
    return(
        <form className='checkout-modal-form' onSubmit={onNext}>
            <div className='checkout-modal-total-container'>
                <div className="checkout-modal-total">Valor total del pedido: $100,000</div>
            </div>
            <br />
            <br />
            <br />
            <div className='checkout-modal-payment-method-contaniner'>
                <PaymentMethodOption id="radio-bill" Icon={BillIcon} label="Efectivo (Contraentrega)" />
                <PaymentMethodOption id="radio-credit" Icon={CreditCardIcon} label="Tarjeta de credito" />
                <PaymentMethodOptionImg id="radio-pse" Img={Pse} label="PSE" />
            </div>
        </form>
    );
}

const CheckoutModalView = ({showModal, onShowModal, step, onNext, onPrev}) => {
    return(
        <Modal 
            showModal={showModal} 
            onShowModal={onShowModal}
            title="Ingresa los datos para la entrega del pedido"
        >
            <div className='checkout-modal-content'>
                <div className="checkout-modal-tab-container">
                    <div className={ step === 1 ? "checkout-modal-tab checkout-modal-tab-active" : "checkout-modal-tab"}>
                        <div className="checkout-modal-tab-number">1.</div>
                        <div className="checkout-modal-tab-text">Datos de ubicación</div>
                    </div>
                    <div className={ step === 2 ? "checkout-modal-tab checkout-modal-tab-active" : "checkout-modal-tab"}>
                        <div className="checkout-modal-tab-number">2.</div>
                        <div className="checkout-modal-tab-text">Forma de pago</div>
                    </div>
                    <div className={ step === 3 ? "checkout-modal-last-tab checkout-modal-tab-active" : "checkout-modal-last-tab"}>
                        <div className="checkout-modal-tab-number">3.</div>
                        <div className="checkout-modal-tab-text">Realizar pago</div>
                    </div>
                </div>
                { step > 1 &&
                    <div className='checkout-modal-back-button-container'>
                        <CustomLink onClick={onPrev}><div className="checkout-modal-back-button"><div className="checkout-modal-back-button-icon">{'<'}</div><div className="checkout-modal-back-button-text">Atras</div></div></CustomLink>
                    </div>
                }
                { step === 1 &&
                    <AddressForm onNext={onNext}/>
                }
                { step === 2 &&
                    <PaymentMethodForm onNext={onNext}/>
                }
                
            </div>
        </Modal>
    );
}

export default CheckoutModalView;