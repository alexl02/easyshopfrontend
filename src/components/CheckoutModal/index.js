import { useState } from 'react';

import CheckoutModalView from "./CheckoutModalView";

const CheckoutModal = () => {

    const [ step, setStep ] = useState(1);

    const handleNext = (e) => {
        e.preventDefault();
        setStep(step + 1);
    }

    const handlePrev = (e) => {
        e.preventDefault();
        setStep(step - 1);
    }

    return(
        <CheckoutModalView
            showModal={true}
            onShowModal={() => {}}
            step={step}
            onNext={handleNext}
            onPrev={handlePrev}
        />
    )
}

export default CheckoutModal;