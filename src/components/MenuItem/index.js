import { useState } from 'react';

import MenuItemView from "./MenuItemView";

const MenuItem = (props) => {

    const [ height, setHeight ] = useState(0);

    const handleCategoryClick = () => {
        setHeight(height === 0 ? 'auto' : 0);
    }

    return(
        <MenuItemView 
            item={props.item} 
            height={height} 
            onCategoryClick={handleCategoryClick}
        />
    );
}

export default MenuItem;