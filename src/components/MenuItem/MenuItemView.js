import { Link } from 'react-router-dom';
import AnimateHeight from 'react-animate-height';
import DefaultImg from '../../img/default.png';
import './MenuItem.css';

const MenuItemView = (props) => {

    const { item, height, onCategoryClick } = props;

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    return(
        <div className="menu-item-container">
            <Link to={{ pathname: "/store/category/" + item.id }} className="menu-item-category" onClick={onCategoryClick} >
                <div className="menu-item-img-container"><img src={item.imgUrl} className="menu-item-img" onError={handleImgError} alt="Imagen del producto" /></div>
                <div className="menu-item-label">{item.name}</div>
            </Link>
            <AnimateHeight duration={ 250 } height={height}>
                <div className="menu-item-subcategory-container">
                    { item.categoryLevel2List.map(
                        (subcategory, index) => {
                            return(
                                <Link key={index} to={{ pathname: "/store/subcategory/" + subcategory.id }} className="menu-item-category">
                                    <div className="menu-item-label">{subcategory.name}</div>
                                </Link>
                            );
                        }
                    ) 
                    }
                </div>
            </AnimateHeight>
        </div>
    );
}

export default MenuItemView;