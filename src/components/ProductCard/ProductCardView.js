import { Link, useNavigate } from 'react-router-dom';
import DefaultImg from '../../img/default.png';
import './ProductCard.css';

const ProductCardView = (props) => {

    const { product, onAdd, onClick, onPlus, onMinus, onChange, qty } = props;
    let navigate = useNavigate();

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    return(
        
        <Link to={{ pathname: "/store/product/" + product.id + "/" + product.price.um, replace: true }} className="product-card-link">
            <div className="product-card">    
                    <div className="product-card-img-container"><img className="product-card-img" src={product.imgUrl} onError={handleImgError} /></div>
                    <div className="product-card-name">{product.name}</div>
                    <div className="product-card-mu">{product.price.umName}</div>
                    <div className="product-card-price">${ product.price.price.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                    <div className="product-card-pum">{product.pumUm + ' a $' + (product.price.price / product.pumContent).toLocaleString("en-US", { maximumFractionDigits: 2 })}</div>
                    <div className="product-card-sku">PLU: {product.erpProduct.sku}</div>
                    { qty < 1 ?
                    <div className="product-card-button-container">
                        <button className="product-card-button" onClick={onAdd} >Agregar</button>
                    </div>
                    :
                    <div className="product-card-qty-container" onClick={onClick}>
                        <button className="product-card-qty-minus" onClick={onMinus}>-</button>
                        <input type="numeric" className="product-card-qty-input" value={qty} onChange={onChange} />
                        <button className="product-card-qty-plus" onClick={onPlus}>+</button>
                    </div>
                    }
            </div>
        </Link>

    );
}

export default ProductCardView;