import { useState, useEffect } from "react";
import ProductCardView from "./ProductCardView"

import { connect } from 'react-redux';
// eslint-disable-next-line
import ShoppingCartReducer from "../../redux/reducers/ShoppingCartReducer";
import { setProductsAction } from "../../redux/actions/ShoppingCartActions";

const ProductCard = (props) => {

    const { ShoppingCartReducer, setProductsAction } = props;

    const [ qty, setQty ] = useState(0);
    
    const { product } = props;

    useEffect(() => {
        const index = ShoppingCartReducer.products.findIndex( p => (p.id === product.id) && (p.price.um === product.price.um));
        if(index > -1) {
            setQty(ShoppingCartReducer.products[index].qty);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        const index = ShoppingCartReducer.products.findIndex( p => (p.id === product.id) && (p.price.um === product.price.um));
        if(index > -1) {
            setQty(ShoppingCartReducer.products[index].qty);
        } else {
            setQty(0);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ShoppingCartReducer.products]);

    const handleAdd = (e) => {
        e.preventDefault();
        let p = { ...product }
        p.qty = 1;
        setProductsAction(p);
        setQty(1);
    }

    const handlePlus = () => {
        let p = { ...product }
        p.qty = qty + 1;
        setProductsAction(p);
        setQty(qty + 1);
    }

    const handleMinus = () => {
        if(qty > 0){
            let p = { ...product }
            p.qty = qty - 1;
            setProductsAction(p);
            setQty(qty - 1);
        }
    }

    const handleChange = (e) => {
        setQty(e.target.value);
    }

    const handleClick = (e) => {
        e.preventDefault();
        
    }

    return(
        <ProductCardView 
            product={product} 
            onAdd={handleAdd}
            onClick={handleClick}
            onPlus={handlePlus}
            onMinus={handleMinus}
            onChange={handleChange}
            qty={qty}
        />
    );
}

const mapStateToProps = ({ ShoppingCartReducer }) => {
    return {
        ShoppingCartReducer
    };
};

const mapDispatchToProps = {
    setProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);