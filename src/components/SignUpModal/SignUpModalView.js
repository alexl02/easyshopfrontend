import Modal from '../Modal';
import Input from '../Input';
import { LoadingOutlined } from '@ant-design/icons';
import './SignUpModal.css';

const SignUpModalView = ({ showModal, onShowSignUpModal, form, onFormChange, onSubmit, tyc, onChangeTyc, tycMsg }) => {
    return(
        <Modal 
            showModal={showModal} 
            onShowModal={onShowSignUpModal}
            title="Registrate"
        >
            <form onSubmit={onSubmit}>
                <div className="form-field">
                    <Input type="text" name="name" label="Nombre(s):" value={form.name} onChange={onFormChange} error={form.error.name} msg={form.msg.name} />
                </div>
                <div className="form-field">
                    <Input type="text" name="lastnames" label="Apellido(s):" value={form.lastnames} onChange={onFormChange} error={form.error.lastnames} msg={form.msg.lastnames} />
                </div>
                <div className="form-field">
                    <Input type="text" name="email" label="Correo electrónico" value={form.email} onChange={onFormChange} error={form.error.email} msg={form.msg.email} />
                </div>
                <div className="form-field">
                    <Input type="number" name="celNumber" label="Número celular" value={form.celNumber} onChange={onFormChange} error={form.error.celNumber} msg={form.msg.celNumber} />
                </div>
                <div className="form-field">
                    <Input type="password" name="password1" label="Contraseña" value={form.password1} onChange={onFormChange} error={form.error.password1} msg={form.msg.password1} />
                </div>
                <div className="form-field">
                    <Input type="password" name="password2" label="Repita a contraseña" value={form.password2} onChange={onFormChange} error={form.error.password2} msg={form.msg.password2} />
                </div>
                <div className="signup-form-tyc-field">
                    <div className="signup-form-field">
                        <input id="tyc" name="tyc" type="checkbox" className="form-chekbox" value={tyc} onChange={onChangeTyc} />
                        <label htmlFor="tyc" className="form-label-checkbox">He leido y acepto los terminos, condiciones y politica de privacidad</label>
                    </div>
                    <div className="signup-tyc-msg">{tycMsg}</div>
                </div>
                { form.loading ?
                    <div className="signup-modal-from-spinner"><LoadingOutlined /></div>
                    :
                    <div className="signup-modal-form-buttons-container">
                        <input type="submit" className="custom-button" value="Aceptar" />
                    </div>
                }
            </form>
        </Modal>
    );
}

export default SignUpModalView;