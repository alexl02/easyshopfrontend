import { useState, useEffect } from 'react';
import SignUpModalView from './SignUpModalView';
import axios from 'axios';
import { httpGet, httpPut } from '../../http';
import ModalView from '../Modal/ModalView';
import { getCustomerSessionAction } from '../../redux/actions/CustomerSessionActions';

import { connect } from 'react-redux';

const SignUpModal = ({ showModal, onShowSignUpModal, getCustomerSessionAction }) => {

    const [ form, setForm ] = useState({
        name: '',
        lastnames: '',
        email: '',
        celNumber: '',
        password1: '',
        password2: '',
        error: {
            name: false,
            lastnames: false,
            email: false,
            celNumber: false,
            password1: false,
            password2: false,
        },
        msg: {
            name: '',
            lastnames: '',
            email: '',
            celNumber: '',
            password1: '',
            password2: '',
        },
        loading: false
    });

    const [ tyc, setTyc ] = useState(false);
    const [ tycMsg, setTycMsg ] = useState("");

    useEffect(() => {

    }, [])

    const handleFormChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const handleChaneTyc = () => {
        setTyc(!tyc);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setForm({...form, loading: true});
        let error = false;
        let msg = "";
        let newForm = {
            ...form,
            error: {
                name: false,
                lastnames: false,
                email: false,
                celNumber: false,
                password1: false,
                password2: false,
            },
            msg: {
                name: '',
                lastnames: '',
                email: '',
                celNumber: '',
                password1: '',
                password2: '',
            },
            loading: false
        }
        setTycMsg("");

        if(form.name.length < 1){
            error = true;
            msg = msg + "Debe ingresar un nombre.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    name: true
                },
                msg: {
                    ...newForm.msg,
                    name: "Debe ingresar un nombre."
                }
            };
        }

        if(form.lastnames.length < 1){
            error = true;
            msg = msg + "Debe ingresar un apellido.\n\n"
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    lastnames: true
                },
                msg: {
                    ...newForm.msg,
                    lastnames: "Debe ingresar un apellido."
                }
            };
        }

        if(form.email.length < 1){
            error = true;
            msg = msg + "Debe ingresar un correo electronico.\n\n"
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    email: true
                },
                msg: {
                    ...newForm.msg,
                    email: "Debe ingresar un correo electronico."
                }
            };
        } else {
            const r = await valField("/customer/email/" + form.email);
            if (r==1) {
                error = true;
                msg = msg + "El correo electronico ya se encuentra registrado.\n\n"
                newForm = {
                    ...newForm,
                    error: {
                        ...newForm.error,
                        email: true
                    },
                    msg: {
                        ...newForm.msg,
                        email: "El correo electronico ya se encuentra registrado."
                    }
                };
            } else if (r == 2) {
                error = true;
                msg = msg + "Error al validar el correo electronico.\n\n"
            }
        }

        if(form.celNumber.length < 1){
            error = true;
            msg = msg + "Debe ingresar un numero celular.\n\n"
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    celNumber: true
                },
                msg: {
                    ...newForm.msg,
                    celNumber: "Debe ingresar un numero celular."
                }
            };
        } else {
            const r = await valField("/customer/cel-number/" + form.celNumber);
            if (r==1) {
                error = true;
                msg = msg + "El numero celular ya se encuentra registrado.\n\n"
                newForm = {
                    ...newForm,
                    error: {
                        ...newForm.error,
                        celNumber: true
                    },
                    msg: {
                        ...newForm.msg,
                        celNumber: "El numero celular ya se encuentra registrado."
                    }
                };
            } else if (r == 2) {
                error = true;
                msg = msg + "Error al validar el numero celular.\n\n"
            }
        }

        if(form.password1.length < 1){
            error = true;
            msg = msg + "Debe ingresar una contraseña.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    password1: true
                },
                msg: {
                    ...newForm.msg,
                    password1: "Debe ingresar una contraseña."
                }
            };
        }

        if(form.password2.length < 1){
            error = true;
            msg = msg + "Debe repetir la contraseña.\n\n"
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    password2: true
                },
                msg: {
                    ...newForm.msg,
                    password2: "Debe repetir la contraseña."
                }
            };
        }

        if(form.password1 !== form.password2){
            error = true;
            msg = msg + "Las contraseñas no coinciden.\n\n"
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    password1: true,
                    password2: true
                },
                msg: {
                    ...newForm.msg,
                    password1: "Debe repetir la contraseña.",
                    password2: "Debe repetir la contraseña."
                }
            };
        }

        if(!tyc) {
            error = true;
            msg = msg + "Debes aceptar nuestros terminos y condiciones.\n\n"
            setTycMsg("Debes aceptar nuestros terminos y condiciones.");
        }

        setForm(newForm);

        if(!error){
            let data = {...form}
            data.password = data.password1;
            data.password1 = null;
            data.password2 = null
            try{
                const resp = await httpPut("/customer", data);
                console.log(resp);
                if(resp.data){
                    localStorage.setItem("sessionId", resp.data.id);
                    getCustomerSessionAction(resp.data.id);
                }
                
                
                setForm({
                    name: '',
                    lastnames: '',
                    email: '',
                    celNumber: '',
                    password1: '',
                    password2: '',
                    error: {
                        name: false,
                        lastnames: false,
                        email: false,
                        celNumber: false,
                        password1: false,
                        password2: false,
                    },
                    msg: {
                        name: '',
                        lastnames: '',
                        email: '',
                        celNumber: '',
                        password1: '',
                        password2: '',
                    },
                    loading: false
                });
                handleOnShowSignUpModal();
            } catch (error) {
                console.log(error);
            }
        } else {
            console.log(msg);
        }
    }

    const handleOnShowSignUpModal = () => {
        setForm({
            name: '',
            lastnames: '',
            email: '',
            celNumber: '',
            password1: '',
            password2: '',
            error: {
                name: false,
                lastnames: false,
                email: false,
                celNumber: false,
                password1: false,
                password2: false,
            },
            msg: {
                name: '',
                lastnames: '',
                email: '',
                celNumber: '',
                password1: '',
                password2: '',
            },
            loading: false
        });
        setTyc(false);
        setTycMsg("");
        onShowSignUpModal();
    }

    const valField = async (url) => {
        try {
            const resp = await httpGet(url);
            if(resp.data){
                return 1;
            }
        } catch (error){
            if (error instanceof axios.AxiosError){
                if((error.response.data.code) && (error.response.data.code !== 'ERROR_DATA_NOT_FOUND')){
                    return 2
                }
            } else {
                return 2
            }
        }
        return 0;
    }

    const cleanErrors = () => {
        setForm({
            error: {
                name: false,
                lastnames: false,
                email: false,
                celNumber: false,
                password1: false,
                password2: false,
            },
            msg: {
                name: '',
                lastnames: '',
                email: '',
                celNumber: '',
                password1: '',
                password2: '',
            }
        });
    }

    return(
        <SignUpModalView showModal={showModal} onShowSignUpModal={handleOnShowSignUpModal} form={form} onFormChange={handleFormChange} onSubmit={handleSubmit} tyc={tyc} onChangeTyc={handleChaneTyc} tycMsg={tycMsg} />
    );
}

const mapStateToProps = ({ }) => {
    return {
        
    };
};

const mapDispatchToProps = {
    getCustomerSessionAction
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUpModal);