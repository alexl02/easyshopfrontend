import './SimpleHeader.css';
import Logo from '../../img/logo.png';
import CustomLink from '../CustomLink';

const SimpleHeaderView = ({ onBack }) => {
    return(
        <div>
            <div className="simple-header-container">
                <CustomLink>
                    <div className="simple-header-logo"><img className="simple-header-logo-img" src={ Logo } /></div>
                </CustomLink>
                <div className='simple-header-button-container'>
                    <button className="custom-button-white" onClick={onBack}>Volver a la tienda</button>
                </div>
            </div>
        </div>
    );
}

export default SimpleHeaderView;