import { useNavigate } from "react-router-dom";
import SimpleHeaderView from "./SimpleHeaderView";

const SimpleHeader = () => {

    const navigate = useNavigate();

    const handleBack = () => {
        navigate('/');
    }

    return(
        <SimpleHeaderView onBack={handleBack} />
    );
}

export default SimpleHeader;