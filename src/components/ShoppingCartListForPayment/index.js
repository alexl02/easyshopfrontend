import ShoppingCartListForPaymentView from "./ShoppingCartListForPaymentView";

import { connect } from 'react-redux';
import { clearProductsAction } from '../../redux/actions/ShoppingCartActions';

const ShoppingCartListForPayment = ({ ShoppingCartReducer }) => {
    
    return(
        <ShoppingCartListForPaymentView 
            ShoppingCartReducer={ShoppingCartReducer}
        />
    );
}

export default ShoppingCartListForPayment;