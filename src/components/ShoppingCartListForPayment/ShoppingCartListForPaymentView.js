import { useState, useEffect } from 'react';
import './ShoppingCartListForPayment.css';
import DefaultImg from '../../img/default.png';

import { connect } from 'react-redux';
import ShoppingCartReducer from "../../redux/reducers/ShoppingCartReducer";
import { setProductsAction } from "../../redux/actions/ShoppingCartActions";

const ShoppingCartListForPaymentItem = ({ product }) => {

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    return(
        <div className="shoppingcart-item-for-payment-container">
            <div className="shoppingcart-item-for-payment">
                <div className="shoppingcart-item-for-payment-img-container"><img className="shoppingcart-item-for-payment-img" src={product.imgUrl} onError={handleImgError}   /></div>
                <div className="shoppingcart-item-for-payment-info">
                    <div className="shoppingcart-item-for-payment-info-name">{product.name}</div>
                    <div className="shoppingcart-item-for-payment-info-price">Precio: ${ product.price.price.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                    <div className="shoppingcart-item-for-payment-info-sku">PLU: {product.erpProduct.sku}</div>
                    
                </div>
                
            </div>
            <div className="shoppingcart-item-for-payment-footer">
                <div className="shoppingcart-item-for-payment-info-total">Cantidad: { product.qty.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                <div className="shoppingcart-item-for-payment-info-total">Valor total: ${ (product.price.price * product.qty).toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
            </div>
        </div>
    );
}

const ShoppingCartListForPaymentView = ({ShoppingCartReducer}) => {

    return (
        <div className="shoppingcart-list-for-payment-container">
            <div className="shoppingcart-list-for-payment-title-container">
                <div className="shoppingcart-list-for-payment-title">Tu pedido</div>
            </div>
            <div className="shoppingcart-item-for-payments">
                {
                    ShoppingCartReducer.products.map( (product, index) => {
                        return(
                            <ShoppingCartListForPaymentItem key={index} product={product} />
                        );
                    } )
                }
                <div style={{ width: '100%', height: '400px' }}></div>
            </div>
            <div className="shoppingcart-item-for-payments-total-container">
                <div className="shoppingcart-list-for-payment-total shoppingcart-list-for-payment-title">Total de pedido: ${ ShoppingCartReducer.total.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
            </div>
        </div>
    );
}

export default ShoppingCartListForPaymentView;