import ModalView from "./ModalView";

const Modal = ({ children, showModal, onShowModal, title }) => {
    return(
        <ModalView showModal={showModal} onShowModal={onShowModal} title={title}>
            {children}
        </ModalView>
    );
}

export default Modal;