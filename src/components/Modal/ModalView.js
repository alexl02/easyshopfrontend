import ReactDOM from 'react-dom';
import './Modal.css'

const ModalView = ({ children, showModal, onShowModal, title, footer }) => {
    return(
        ReactDOM.createPortal(
            <div className={ showModal ? "modal-container" : "modal-container-disabled"}>
                <div className="modal-content">
                    <div className="modal-header">
                        <div className="modal-header-title">{title}</div>
                        <button className="modal-header-close-button" onClick={onShowModal}>x</button>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer">
                        {footer}
                    </div>
                </div>
            </div>
        ,document.getElementById('modal'))
    );
}

export default ModalView;