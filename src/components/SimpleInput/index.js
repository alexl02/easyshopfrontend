import SimpleInputView from "./SimpleInputView";

const SimpleInput = ({ type, error, msg, name, value, onChange, placeholder }) => {
    return(<SimpleInputView type={type} error={error} msg={msg} name={name} value={value} onChange={onChange} placeholder={placeholder} />);
}

export default SimpleInput;