import './SimpleInput.css';

const SimpleInputView = ({ type, error, msg, name, value, placeholder, onChange }) => {
    return(
        <div className="simple-input-main-container">
            <div className="simple-input-container">
                <input type={type} className={ !error ? "simple-input" : "simple-input-error"} name={name} value={value} onChange={onChange} placeholder={placeholder} />
            </div>
            { (msg !== undefined) && (msg !== null) &&
                <div className="simple-input-msg">{msg}</div>
            }
        </div>
    );
}

export default SimpleInputView;