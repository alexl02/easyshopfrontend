import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import HomeLayout from './pages/HomeLayout';
import Checkout from './pages/Checkout';
import Splash from './pages/Splash';
import './App.css';

import { connect } from 'react-redux';
// eslint-disable-next-line
import HomeDataReducer from './redux/reducers/HomeDataReducer';
import { getHomeDataAction } from './redux/actions/HomeDataActions';
import { getCustomerSessionAction, cleanCustomerSessionAction } from './redux/actions/CustomerSessionActions';

function App({ HomeDataReducer, getHomeDataAction, getCustomerSessionAction, cleanCustomerSessionAction }) {

  const [ showSplash, setShowSplash ] = useState(true);

  useEffect(() => {
    //const t = setTimeout(() => { setOnSplash(false) }, 3000);
    
    const getInfo = async () => {
      if(localStorage.getItem("sessionId")){
        await getCustomerSessionAction(localStorage.getItem("sessionId"));
      } else {
        cleanCustomerSessionAction();
      }
      await getHomeDataAction();
      setShowSplash(false);
    }

    getInfo();

    return () => {
      //clearTimeout(t);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="App">
      { showSplash 
        ? <Splash />
        :
          <BrowserRouter basename={"/listao"}>
            <Routes>
              <Route path="/checkout" element={<Checkout />} />
              <Route path="/store/*" element={<HomeLayout />} />
              {/*<Route path="/" element={<Splash />} />*/}
              <Route path="/" element={<Navigate to="/store" replace />} />
            </Routes>
          </BrowserRouter>
      }
    </div>
  );
}

const mapStateToProps = ({ HomeDataReducer }) => {
  return {
      HomeDataReducer
  };
};

const mapDispatchToProps = {
  getHomeDataAction,
  getCustomerSessionAction,
  cleanCustomerSessionAction
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
