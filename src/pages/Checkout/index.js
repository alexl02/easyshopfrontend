import { useState, useEffect } from 'react';
import axios from 'axios';
import { httpPut } from '../../http';
import { useNavigate } from 'react-router-dom';
import CheckoutView from "./CheckoutView";

import { connect } from 'react-redux';
// eslint-disable-next-line
import ShoppingCartReducer from '../../redux/reducers/ShoppingCartReducer';
import CustomerSessionReducer from '../../redux/reducers/CustomerSessionReducer';
import { clearProductsAction } from '../../redux/actions/ShoppingCartActions';


const Checkout = ({ShoppingCartReducer, CustomerSessionReducer, clearProductsAction}) => {

    const [ step, setStep ] = useState(1);
    const [ locationForm, setLocationForm ] = useState({
        addressStreetType: 'Calle',
        addressStreetName: '',
        addressStreetNumber: '',
        addressStreet2Name: '',
        addressNumber: '',
        locationDetails: '',
        locationTelNumber: '',
        locationZone: '',
        error: {
            addressStreetName: false,
            addressStreet2Name: false,
            addressNumber: false,
            locationDetails: false,
            locationTelNumber: false,
            locationZone: false,
        },
        msg: {
            addressStreetName: '',
            addressStreet2Name: '',
            addressNumber: '',
            locationDetails: '',
            locationTelNumber: '',
            locationZone: '',
        },
        loading: false
    });

    const [ result, setResult ] = useState({
        loading: true,
        successful: false,
        msg: ''
    });

    const navigate = useNavigate();

    useEffect(() => {
        if (ShoppingCartReducer.products.length < 1) {
            navigate('/');
        }

        if(!localStorage.getItem("sessionId")){
            navigate('/')
        }
    }, []);

    const handleNext = () => {
        setStep(step + 1);
    }

    const handlePrev = () => {
        setStep(step - 1);
    }

    const handleLocationFormChange = (e) => {
        setLocationForm({
            ...locationForm,
            [e.target.name]: e.target.value
        });
        /*if(e.target.name !== "addressStreetType") {
            
        }*/
    }

    const handleLocationFormSubmit = (e) => {
        e.preventDefault();
        if(!validateLocationForm()) {
            handleNext();
        }
    }

    const validateLocationForm = () => {
        let error = false;
        let msg = "";
        let newForm = {
            ...locationForm,
            error: {
                addressStreetName: false,
                addressStreet2Name: false,
                addressNumber: false,
                locationDetails: false,
                locationTelNumber: false,
                locationZone: false,
            },
            msg: {
                addressStreetName: '',
                addressStreet2Name: '',
                addressNumber: '',
                locationDetails: '',
                locationTelNumber: '',
                locationZone: '',
            }
        };

        if(locationForm.addressStreetName < 1){
            error = true;
            msg = msg + "Debe ingresar una dirección valida.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    addressStreetName: true
                },
                msg: {
                    ...newForm.msg,
                    addressStreetName: "Debe ingresar una dirección valida."
                }
            };
        }

        if(locationForm.addressStreet2Name < 1){
            error = true;
            msg = msg + "Debe ingresar una dirección valida.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    addressStreetName: true,
                    addressStreet2Name: true
                },
                msg: {
                    ...newForm.msg,
                    addressStreetName: "Debe ingresar una dirección valida."
                }
            };
        }

        if(locationForm.addressNumber < 1){
            error = true;
            msg = msg + "Debe ingresar una dirección valida.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    addressStreetName: true,
                    addressNumber: true
                },
                msg: {
                    ...newForm.msg,
                    addressStreetName: "Debe ingresar una dirección valida."
                }
            };
        }

        if(locationForm.locationTelNumber < 1){
            error = true;
            msg = msg + "Debe ingresar un numero telefónico.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    locationTelNumber: true
                },
                msg: {
                    ...newForm.msg,
                    locationTelNumber: "Debe ingresar un numero telefónico."
                }
            };
        } else if (locationForm.locationTelNumber < 10){
            error = true;
            msg = msg + "Debe ingresar un numero telefónico valido.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    locationTelNumber: true
                },
                msg: {
                    ...newForm.msg,
                    locationTelNumber: "Debe ingresar un numero telefónico valido."
                }
            };
        }

        if(locationForm.locationZone < 1){
            error = true;
            msg = msg + "Debe ingresar un barrio.\n\n";
            newForm = {
                ...newForm,
                error: {
                    ...newForm.error,
                    locationZone: true
                },
                msg: {
                    ...newForm.msg,
                    locationZone: "Debe ingresar un barrio."
                }
            };
        }
        setLocationForm(newForm);
        return error;
    }

    const handlePaymentFormSubmit = async (e) => {
        e.preventDefault();
        handleNext();
        setResult({...result, loading: true});
        
        let dataDetails = [];

        for (const product of ShoppingCartReducer.products){
            let detail = {
                productId: product.id,
                price: product.price.price,
                discountPrice: product.price.discountPrice,
                mu: product.price.um,
                qty: product.qty
            }
            dataDetails.push(detail);
        }
        
        let data = {
            address: locationForm.addressStreetType + ' ' + locationForm.addressStreetName + ' # ' + locationForm.addressStreet2Name + ' - ' + locationForm.addressNumber,
            addressDetails: locationForm.locationDetails,
            telephoneNumber: locationForm.locationTelNumber,
            zone: locationForm.locationZone,
            branchId: 1,
            customerId: CustomerSessionReducer.customerSessionData.customer.id,
            orderDetails: dataDetails
        }
        try{
            const resp = await httpPut('/order', data);
            if(resp.data){

                setResult({
                    ...result,
                    loading: false,
                    successful: true,
                    msg: 'Tu pedido fue recibido correctamente, te hemos enviado un correo con el resumen de la transacción, nos comunicaremos contigo para coordinar la entrega.'
                });
                clearProductsAction();
                
            }

        } catch (error){
            setResult({
                ...result,
                loading: false,
                successful: false,
                msg: 'Ocurrio un error al enviar tu pedido, comunicate con nosotros para ayudarte.'
            });
        }
    }

    const handleBackStore = () => {
        navigate('/');
    }

    return(
        <CheckoutView 
            step={step} 
            onLocationFormSubmit={handleLocationFormSubmit}
            onPrev={handlePrev} 
            ShoppingCartReducer={ShoppingCartReducer} 
            locationForm={locationForm}
            onLocationFormChange={handleLocationFormChange} 
            onPaymentFormSubmit={handlePaymentFormSubmit}
            result={result}
            onBackStore={handleBackStore}
        />
    );
}

const mapStateToProps = ({ ShoppingCartReducer, CustomerSessionReducer }) => {
    return {
        ShoppingCartReducer,
        CustomerSessionReducer
    };
};

const mapDispatchToProps = {
    clearProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);