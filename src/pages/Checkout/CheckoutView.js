import SimpleHeader from '../../components/SimpleHeader';
import SimpleInput from '../../components/SimpleInput';
import CustomLink from '../../components/CustomLink';
import PseIcon from '../../img/boton-pse.png';
import { LoadingOutlined, CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons';
import './Checkout.css';
import ShoppingCartListForPayment from '../../components/ShoppingCartListForPayment';

const LocationForm = ({ onSubmit, locationForm, onLocationFormChange }) => {
    return(
        <form className='checkout-form' onSubmit={onSubmit}>
            <div className="checkout-title">Ingresa los datos donde se entragará tu pedido.</div>
            <div className="checkout-location-field-container">
                <div className="checkout-location-container">
                    <select className="checkout-location-select" name="addressStreetType" value={locationForm.addressStreetType} onChange={onLocationFormChange}>
                        <option value="Calle">Calle</option>
                        <option value="Carrera">Carrera</option>
                        <option value="Diagonal">Diagonal</option>
                        <option value="Transversal">Transversal</option>
                        <option value="Callejon">Callejon</option>
                    </select>
                    <div className='checkout-location-input-street-container'>
                        <SimpleInput type="text" name="addressStreetName" value={locationForm.addressStreetName} onChange={onLocationFormChange} error={locationForm.error.addressStreetName} />
                    </div>
                    <div className='checkout-location-input-separator-char'>#</div>
                    <div className='checkout-location-input-street-container'>
                        <SimpleInput type="text" name="addressStreet2Name" value={locationForm.addressStreet2Name} onChange={onLocationFormChange} error={locationForm.error.addressStreet2Name} />
                    </div>
                    <div className='checkout-location-input-separator-char'>-</div>
                    <div className='checkout-location-input-street-container'>
                        <SimpleInput type="text" name="addressNumber" value={locationForm.addressNumber} onChange={onLocationFormChange} error={locationForm.error.addressNumber} />
                    </div>
                </div>
                { locationForm.error.addressStreetName &&
                    <div className="checkout-location-msg-container">
                        { locationForm.msg.addressStreetName }
                    </div>
                }
            </div>
            <div className="checkout-location-details-container">
                <SimpleInput type="text" name="locationDetails" placeholder='Conjunto, Torre, Apto.' value={locationForm.locationDetails} onChange={onLocationFormChange} />
            </div>
            <div className="checkout-location-tel-container">
                <SimpleInput type="text" name="locationTelNumber" placeholder='Número telefónico de contacto.' value={locationForm.locationTelNumber} onChange={onLocationFormChange} error={locationForm.error.locationTelNumber} msg={locationForm.msg.locationTelNumber} />
            </div>
            <div className="checkout-location-zone-container">
                <SimpleInput type="text" name="locationZone" placeholder='Barrio.' value={locationForm.locationZone} onChange={onLocationFormChange} error={locationForm.error.locationZone} msg={locationForm.msg.locationZone} />
            </div>
            <br />
            <br />
            <div className="checkout-location-form-buttons-container">
                <input type="submit" className="custom-button" value="Siguiente" />
            </div>
        </form>
    );
}

const BillIcon = () => {
    return(
        <svg className="checkout-modal-payment-method-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M400 256C400 317.9 349.9 368 288 368C226.1 368 176 317.9 176 256C176 194.1 226.1 144 288 144C349.9 144 400 194.1 400 256zM272 224V288H264C255.2 288 248 295.2 248 304C248 312.8 255.2 320 264 320H312C320.8 320 328 312.8 328 304C328 295.2 320.8 288 312 288H304V208C304 199.2 296.8 192 288 192H272C263.2 192 256 199.2 256 208C256 216.8 263.2 224 272 224zM0 128C0 92.65 28.65 64 64 64H512C547.3 64 576 92.65 576 128V384C576 419.3 547.3 448 512 448H64C28.65 448 0 419.3 0 384V128zM48 176V336C83.35 336 112 364.7 112 400H464C464 364.7 492.7 336 528 336V176C492.7 176 464 147.3 464 112H112C112 147.3 83.35 176 48 176z"/></svg>
    );
}

const CreditCardIcon = () => {
    return(
        <svg className="checkout-modal-payment-method-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M168 336C181.3 336 192 346.7 192 360C192 373.3 181.3 384 168 384H120C106.7 384 96 373.3 96 360C96 346.7 106.7 336 120 336H168zM360 336C373.3 336 384 346.7 384 360C384 373.3 373.3 384 360 384H248C234.7 384 224 373.3 224 360C224 346.7 234.7 336 248 336H360zM512 32C547.3 32 576 60.65 576 96V416C576 451.3 547.3 480 512 480H64C28.65 480 0 451.3 0 416V96C0 60.65 28.65 32 64 32H512zM512 80H64C55.16 80 48 87.16 48 96V128H528V96C528 87.16 520.8 80 512 80zM528 224H48V416C48 424.8 55.16 432 64 432H512C520.8 432 528 424.8 528 416V224z"/></svg>
    );
}

const PaymentMethodOption = ({ id, Icon, label, isSelected }) => {
    return(
        <div className='checkout-payment-method-option'>
            <div className="checkout-payment-method-label">
                    <div className="checkout-payment-method-icon-container"><Icon /></div>
                    <div className='checkout-payment-method-label-text'>{label}</div>
            </div>
        </div>
    );
}

const PaymentMethodOptionImg = ({ id, Img, label }) => {
    return(
        <div className='checkout-payment-method-option'>
            <input type="radio" id={id} />
            <label htmlFor={id} >
                <div className="checkout-payment-method-label">
                    <div className="checkout-payment-method-icon-container"><img className='checkout-modal-payment-method-img' src={Img} alt="boton pse" /></div>
                    <div className='checkout-payment-method-label-text'>{label}</div>
                </div>
            </label>
        </div>
    );
}

const PaymentForm = ({onSubmit, total}) => {
    return(
        <form className='checkout-form' onSubmit={onSubmit}>
            <div className='checkout-total-container'>
                <div className="checkout-total">Valor total del pedido: ${total.toLocaleString("en-US", { maximumFractionDigits: 2 })}</div>
            </div>
            <br />
            <br />
            <br />
            <div className='checkout-payment-method-contaniner'>
                <PaymentMethodOption id="radio-bill" Icon={BillIcon} label="Efectivo (Contraentrega)" />
                {/*<PaymentMethodOption id="radio-credit" Icon={CreditCardIcon} label="Tarjeta de credito" />
                <PaymentMethodOptionImg id="radio-pse" Img={PseIcon} label="PSE" />*/}
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div className="checkout-location-form-buttons-container">
                <input type="submit" className="custom-button" value="Siguiente" />
            </div>
        </form>
    );
}

const endPage = () => {
    return(
        <div className=''>
            <div></div>
        </div>
    )
}

const CheckoutView = ({ step, onPrev, ShoppingCartReducer, locationForm, onLocationFormChange, onLocationFormSubmit, onPaymentFormSubmit, result, onBackStore }) => {
    return(
        <div className='checkout-page'>
            <SimpleHeader />
            <div className="checkout-container">
                <div className='checkout-main-container'>
                    <div className="checkout-tab-container">
                        <div className={ step === 1 ? "checkout-tab checkout-tab-active" : "checkout-tab"}>
                            <div className="checkout-tab-number">1.</div>
                            <div className="checkout-tab-text">Datos de ubicación</div>
                        </div>
                        <div className={ step === 2 ? "checkout-tab checkout-tab-active" : "checkout-tab"}>
                            <div className="checkout-tab-number">2.</div>
                            <div className="checkout-tab-text">Realizar pago</div>
                        </div>
                        <div className={ step === 3 ? "checkout-last-tab-active" : "checkout-last-tab"}>
                            <div className="checkout-tab-number">3.</div>
                            <div className="checkout-tab-text">Finalizar</div>
                        </div>
                    </div>
                    { (step > 1) && (step < 3) &&
                        <div className='checkout-back-button-container'>
                            <CustomLink onClick={onPrev}><div className="checkout-modal-back-button"><div className="checkout-modal-back-button-icon">{'<'}</div><div className="checkout-back-button-text">Atras</div></div></CustomLink>
                        </div>
                    }
                    <div className="checkout-content-container">
                        { step === 1 &&
                            <LocationForm  locationForm={locationForm} onLocationFormChange={onLocationFormChange} onSubmit={onLocationFormSubmit} />
                        }
                        { step === 2 &&
                            <PaymentForm  onSubmit={onPaymentFormSubmit} total={ShoppingCartReducer.total} />
                        }
                        { step === 3 &&
                            <div className="checkout-result-container">
                                { result.loading ?
                                    <div>
                                        <div className="checkout-result-text">Estamos procesando tu pedido...</div>
                                        <div className="checkout-result-spinner"><LoadingOutlined /></div>
                                    </div>
                                :
                                    <div className='checkout-result-box'>
                                        { result.successful
                                            ? <div className="checkout-result-icon"><CheckCircleTwoTone twoToneColor="#52c41a" /></div>
                                            : <div className="checkout-result-icon"><CloseCircleTwoTone twoToneColor="#ff0000" /></div>
                                        }
                                        <div className="checkout-result-text">{result.msg}</div>
                                        <br />
                                        <br />
                                        <br />
                                        <input type="button" className="custom-button" value="Volver a la tienda" onClick={onBackStore} />
                                    </div>
                                }
                            </div>
                        }
                    </div>
                </div>
                { step < 3 &&
                    <ShoppingCartListForPayment ShoppingCartReducer={ShoppingCartReducer} />
                }
            </div>
        </div>
    );
}

export default CheckoutView;