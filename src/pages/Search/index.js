import { useEffect, useState } from "react";
import SearchView from "./SearchView";
import { useParams } from "react-router-dom";
import { httpGet } from "../../http";

const Search = (props) => {

    const [ searchResult, setSearchResult ] = useState([]);
    
    const { word } = useParams();

    useEffect(() => {
        doSearch();
    }, []);

    useEffect(() => {
        doSearch();
    }, [word]);

    const doSearch = async () => {
        try {
            const resp = await httpGet("/product/app/search/" + word);
            if(resp.data){
                setSearchResult(resp.data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            { searchResult !== null &&
                <SearchView search={{ name: word, products: searchResult }} />
            }
        </>
    );
}

export default Search;