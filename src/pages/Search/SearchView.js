import ProductCard from '../../components/ProductCard'
import './Search.css';

const SearchView = (props) => {

    const { search } = props;

    return(
        <div className="search-container">
            <div className="search-header">
                <div className="search-name">Busqueda: {search.name}</div>
            </div>
            <div className="search-products-container">
            { search.products.map(
                (product, index) => {
                    return(
                        <ProductCard key={index} product={product} />
                    );
                }
            )}
            </div>
        </div>
    );
}

export default SearchView;