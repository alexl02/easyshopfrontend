import ProductCard from '../../components/ProductCard'
import './Category.css';

const CategoryView = (props) => {

    const { category } = props;

    return(
        <div className="category-container">
            <div className="category-header">
                <div className="category-name">{category.name}</div>
            </div>
            <div className="category-products-container">
            { category.products.map(
                (product, index) => {
                    return(
                        <ProductCard key={index} product={product} />
                    );
                }
            )}
            </div>
        </div>
    );
}

export default CategoryView;