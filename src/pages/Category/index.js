import { useEffect, useState } from "react";
import CategoryView from "./CategoryView";
import { useParams } from "react-router-dom";
import { httpGet } from "../../http";

const Category = (props) => {

    const [ category, setCategory ] = useState(null);
    
    const { categoryId } = useParams();

    useEffect(() => {
        getCategory();
    }, []);

    useEffect(() => {
        getCategory();
    }, [categoryId]);

    const getCategory = async () => {
        try {
            console.log(categoryId);
            const resp = await httpGet("/product/app/category/" + categoryId);
            if(resp.data){
                setCategory(resp.data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            { category !== null &&
                <CategoryView category={category} />
            }
        </>
    );
}

export default Category;