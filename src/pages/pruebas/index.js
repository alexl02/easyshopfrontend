import { useEffect, useState, useRef } from 'react';
import AnimateHeight from 'react-animate-height';
import './pruebas.css';

const Pruebas = (props) => {

    const [ height, setHeight ] = useState(0);
    
    const handleClick = () => {
        setHeight(height === 0 ? 'auto' : 0);
    }

    return(
        <div class="container">
            <button className="button" onClick={handleClick} >Abrir</button>
            <AnimateHeight duration={ 250 } height={height}>
                <div>
                    <div class="option">opcion1</div>
                    <div class="option">opcion2</div>
                    <div class="option">opcion3</div>
                    <div class="option">opcion4</div>
                </div>
            </AnimateHeight>
        </div>
    );
}

export default Pruebas;