import { useEffect } from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { Link } from 'react-router-dom';
import './Product.css';
import ProductCard from '../../components/ProductCard';
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import DefaultImg from '../../img/default.png';
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";

const ProductView = (props) => {

    const { product, muProducts, onAdd, onPlus, onMinus, onChange, qty } = props;
    const navigate = useNavigate();
    
    useEffect(() => {
        console.log("PRODUCT!!!");
    }, []);

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    const handleHome = () => {
        console.log("CLICK!!");
        navigate('/');
    }

    return(
        <div className="product-main-container">
            { (product.id !== undefined && product.id !== null) &&
            <>
            <div className="product-view-container">
                <div className="product-img-container">
                    <img src={product.imgUrl} className="product-img" onError={handleImgError} />
                </div>
                <div className="product-container">
                    <div className="product-info-container">
                        <div className="product-name">{product.name}</div>
                        <div className="product-mu">{product.price.umName}</div>
                        <div className="product-price">${ product.price.price.toLocaleString("en-US", { maximumFractionDigits: 2 }) }</div>
                        <div className="product-pum">{product.pumUm + ' a $' + (product.price.price / product.pumContent).toLocaleString("en-US", { maximumFractionDigits: 2 })}</div>
                        <div className="product-sku">PLU: {product.erpProduct.sku}</div>
                        { qty < 1 ?
                         <div className="product-button-container">
                            <button className="product-button" onClick={onAdd}>Agregar</button>
                        </div>
                        :
                        <div className="product-qty-container" >
                            <button className="product-qty-minus" onClick={onMinus}>-</button>
                            <input type="numeric" className="product-qty-input" value={qty} onChange={onChange} />
                            <button className="product-qty-plus" onClick={onPlus}>+</button>
                        </div>
                        }
                    </div>
                </div>
            </div>
            { muProducts.length > 0 &&
            <div className="product-mu-container">
                <div className="product-mu-label">Otras presentaciones:</div>                
                <Swiper
                    slidesPerView={'auto'}
                    spaceBetween={10}
                    className="mySwiper"
                    setWrapperSize={true}
                >
                    { muProducts.map(
                        (product, index) => {
                            return(
                                <SwiperSlide key={index} id="mySwiperSlide">
                                    <ProductCard  product={product} />
                                </SwiperSlide>
                            );
                        }
                    )}    
                </Swiper>
            </div>
            }
            </>
            }
        </div>
    );
}

export default ProductView;