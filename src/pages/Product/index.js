import { useState, useEffect } from 'react';
import { useParams, useNavigate, useLocation } from "react-router-dom";
import ProductView from "./ProductView";
import { httpGet } from "../../http";

import { connect } from 'react-redux';
import ShoppingCartReducer from "../../redux/reducers/ShoppingCartReducer";
import { setProductsAction } from "../../redux/actions/ShoppingCartActions";

const Product = (props) => {

    const { ShoppingCartReducer, setProductsAction } = props;

    const [ qty, setQty ] = useState(0);
   
    const [ product, setProduct ] = useState({});
    const [ muProducts, setMuProducts ] = useState([]);
    const { productId, mu } = useParams();
    let navigate = useNavigate();
    let location = useLocation();

    useEffect(() => {
        console.log("PRODUCT INDEX");
        //handleRefresh(productId, mu);
        getProduct(productId, mu);
        getMuProducts(productId);
    }, [])

    useEffect(() => {
        console.log("refresh");
        getProduct(productId, mu);
        getMuProducts(productId);
    }, [ location ]);

    const getProduct = async (productId, mu) => {
        try {
            console.log(productId)
            const resp = await httpGet("/product/app/id/" + productId + "/" + mu);
            if(resp.data){
                setProduct(resp.data);

                const index = ShoppingCartReducer.products.findIndex( p => (p.id === resp.data.id) && (p.price.um === resp.data.price.um));
                if(index > -1) {
                    setQty(ShoppingCartReducer.products[index].qty);
                }

            }
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        const index = ShoppingCartReducer.products.findIndex( p => (p.id === product.id) && (p.price.um === product.price.um));
        console.log(index);
        if(index > -1) {
            setQty(ShoppingCartReducer.products[index].qty);
        } else {
            setQty(0);
        }
    }, [ShoppingCartReducer.products]);

    const getMuProducts = async (productId) => {
        try {
            console.log(productId)
            const resp = await httpGet("/product/mu/all/" + productId);
            if(resp.data){
                setMuProducts(resp.data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handleAdd = (e) => {
        e.preventDefault();
        let p = { ...product }
        p.qty = 1;
        setProductsAction(p);
        setQty(1);
    }

    const handlePlus = () => {
        let p = { ...product }
        p.qty = qty + 1;
        setProductsAction(p);
        setQty(qty + 1);
    }

    const handleMinus = () => {
        if(qty > 0){
            let p = { ...product }
            p.qty = qty - 1;
            setProductsAction(p);
            setQty(qty - 1);
        }
    }

    const handleChange = (e) => {
        setQty(e.target.value);
    }

    return(
        <>
        {
            (product.id !== undefined || product.id !== null) ? <ProductView 
                product={product} 
                muProducts={muProducts} 
                onAdd={handleAdd}
                onPlus={handlePlus}
                onMinus={handleMinus}
                onChange={handleChange}
                qty={qty}
            /> : <div></div>
        }
        </>
    );
}

const mapStateToProps = ({ ShoppingCartReducer }) => {
    return {
        ShoppingCartReducer
    };
};

const mapDispatchToProps = {
    setProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);