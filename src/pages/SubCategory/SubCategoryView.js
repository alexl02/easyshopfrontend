import ProductCard from '../../components/ProductCard'
import './SubCategory.css';

const SubCategoryView = (props) => {

    const { subCategory } = props;

    return(
        <div className="subcategory-container">
            <div className="subcategory-header">
                <div className="subcategory-name">{subCategory.name}</div>
            </div>
            <div className="subcategory-products-container">
            { subCategory.products.map(
                (product, index) => {
                    return(
                        <ProductCard key={index} product={product} />
                    );
                }
            )}
            </div>
        </div>
    );
}

export default SubCategoryView;