import { useEffect, useState } from "react";
import SubCategoryView from "./SubCategoryView";
import { useParams } from "react-router-dom";
import { httpGet } from "../../http";

const SubCategory = (props) => {

    const [ subCategory, setSubCategory ] = useState(null);
    
    const { subCategoryId } = useParams();

    useEffect(() => {
        getSubCategory();
    }, []);

    useEffect(() => {
        getSubCategory();
    }, [subCategoryId]);

    const getSubCategory = async () => {
        try {
            console.log(subCategoryId);
            const resp = await httpGet("/product/app/subcategory/" + subCategoryId);
            if(resp.data){
                setSubCategory(resp.data);
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            { subCategory !== null &&
                <SubCategoryView subCategory={subCategory} />
            }
        </>
    );
}

export default SubCategory;