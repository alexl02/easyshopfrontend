import ProductCard from '../../components/ProductCard';
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from "swiper";
import { Link } from 'react-router-dom';
import './Home.css';
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import ImgRutasListao from '../../img/rutas_listao.png';
import ImgCamionNaranja from '../../img/camion_naranja.png';
import ImgLabelListaoChao from '../../img/label_listao_chao.png';



const HomeView = (props) => {

    const { categories } = props;

    return(
        <div className="home-container">
            <div className="home-routes-banner-container">
                <div className="home-routes-banner-row-1">
                    <img className="home-routes-banner-img" src={ImgRutasListao} alt=""></img>
                    <img className="home-routes-banner-img" src={ImgCamionNaranja} alt=""></img>
                </div>
                <div className="home-routes-banner-row-2">
                    <img src={ImgLabelListaoChao} alt=""></img>
                </div>
            </div>
            <div className="home-categories-container" >
                {
                    categories.map(
                        (category, index) => {
                            return(
                                <div className="home-category-container" key={index}>
                                    <div className="home-category-header">
                                        <div className="home-category-name">{category.name}</div>
                                        <div className="home-category-link-container"><Link to={ { pathname: '/category/' + category.id } } className="home-category-link">Ver todos</Link></div>
                                    </div>
                                    <div className="home-products-container">
                                    <Swiper
                                        slidesPerView={'auto'}
                                        spaceBetween={10}
                                        navigation={true}
                                        modules={[Navigation]}
                                        className="mySwiper"
                                        setWrapperSize={true}
                                    >
                                            { category.products.map(
                                                (product, index) => {
                                                    return(
                                                        <SwiperSlide key={index} id="mySwiperSlide">
                                                            <ProductCard product={product} />
                                                        </SwiperSlide>
                                                    );
                                                }
                                            )}
                                        </Swiper>
                                    </div>
                                </div>
                            );
                        }
                    )
                }
            </div>
        </div>
    );
}

export default HomeView;