import { useEffect } from "react";
import HomeView from "./HomeView";

import { connect } from 'react-redux';
// eslint-disable-next-line
import HomeDataReducer from '../../redux/reducers/HomeDataReducer';
import { getHomeDataAction } from '../../redux/actions/HomeDataActions';

const Home = ({ HomeDataReducer, getHomeDataAction }) => {

    useEffect(() => {
        //getHomeDataAction();
        console.log("HOME");
    }, []);

    return(
        <HomeView categories={HomeDataReducer.productCategories}/>
    );
}

const mapStateToProps = ({ HomeDataReducer }) => {
    return {
        HomeDataReducer
    };
};

const mapDispatchToProps = {
    getHomeDataAction
  }

export default connect(mapStateToProps)(Home);