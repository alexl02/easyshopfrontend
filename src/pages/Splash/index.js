import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import SplashView from "./SplashView";

import { connect } from 'react-redux';
// eslint-disable-next-line
import HomeDataReducer from '../../redux/reducers/HomeDataReducer';
import { getHomeDataAction } from '../../redux/actions/HomeDataActions';
import { getCustomerSessionAction, cleanCustomerSessionAction } from '../../redux/actions/CustomerSessionActions';

const Splash = ({ getHomeDataAction, getCustomerSessionAction, cleanCustomerSessionAction }) => {    
    useEffect(() => {
        /*if(localStorage.getItem("sessionId")){
            getCustomerSessionAction(localStorage.getItem("sessionId"));
        } else {
            cleanCustomerSessionAction();
        }
        getHomeDataAction();*/

        //const t = setTimeout(() => { navigate('/store'); }, 3000);
    
        
        /*return () => {
            clearTimeout(t);
        }*/
    }, []);

    useEffect(() => {
        /*if((!HomeDataReducer.loading)){
            navigate("/store");
        }*/
    }, [HomeDataReducer]);

    return(
        <SplashView />
    )
}

const mapStateToProps = ({ HomeDataReducer }) => {
    return {
        HomeDataReducer
    };
  };
  
  const mapDispatchToProps = {
    getHomeDataAction,
    getCustomerSessionAction,
    cleanCustomerSessionAction
  }

export default connect(mapStateToProps, mapDispatchToProps)(Splash);