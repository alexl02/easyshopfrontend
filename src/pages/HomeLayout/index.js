import { useState, useEffect } from 'react';
import { httpGet } from '../../http';
import { useNavigate } from 'react-router';
import HomeLayoutView from './HomeLayoutView';

import { connect } from 'react-redux';
// eslint-disable-next-line
import ShoppingCartReducer from '../../redux/reducers/ShoppingCartReducer';
import { clearProductsAction } from '../../redux/actions/ShoppingCartActions';
// eslint-disable-next-line
import HomeDataReducer from '../../redux/reducers/HomeDataReducer';
// eslint-disable-next-line
import CustomerSessionReducer from '../../redux/reducers/CustomerSessionReducer';
// eslint-disable-next-line
import { cleanCustomerSessionAction } from '../../redux/actions/CustomerSessionActions';
import { getHomeDataAction } from '../../redux/actions/HomeDataActions';


const HomeLayout = (props) => {

    const { clearProductsAction, ShoppingCartReducer, HomeDataReducer, CustomerSessionReducer, cleanCustomerSessionAction, getHomeDataAction } = props;
    
    const navigate = useNavigate();

    const [ showShoppingCart, setShowShoppingCart ] = useState(false);
    const [ showSignUpModal, setShowSignUpModal ] = useState(false);
    const [ showLoginModal, setShowLoginModal ] = useState(false);
    const [ showProfileMenu, setShowProfileMenu ] = useState(false);

    const [ bottomBarStatus, setBottomBarStatus ] = useState({
        showHome: true,
        showMenu: false,
        showProfile: false
    });

    const [ searchWord, setSearchWord ] = useState('');
    const [ searchAutocomplete, setSearchAutocomplete ] = useState({ products: [] });

    useEffect(() => {
        
    }, []);
    
    useEffect(() => {
            handleAutocomplete(searchWord);
    }, [searchWord])

    const handleHomeClick = () => {
        setBottomBarStatus({
            showHome: true,
            showMenu: false,
            showProfile: false
        });
    }
    
    const handleCategoriesClick = () => {
        setBottomBarStatus({
            showHome: false,
            showMenu: true,
            showProfile: false
        });
    }

    const handleProfileClick = () => {
        setBottomBarStatus({
            showHome: false,
            showMenu: false,
            showProfile: true
        });
        handleShowLoginModal();
    }

    const handleSearchChange = async (e) => {
        setSearchWord(e.target.value);
    }

    const handleSearchClick = () => {
        navigate('/store/search/' + searchWord);
        setSearchAutocomplete({ products: [] });
    }

    const handleSearchAutocompleteClean = () => {
        setSearchAutocomplete({ products: [] });
    }

    const handleAutocomplete = async (word) => {
        try {
            const resp = await httpGet("/product/app/search/autocomplete/" + word);
            if(resp.data){
                setSearchAutocomplete({ products: resp.data });
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handleShowSignUpModal = () => {
        setShowSignUpModal(!showSignUpModal);
    }

    const handleShowLoginModal = () => {
        setShowLoginModal(!showLoginModal);
    }

    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleShowShoppingCart = () => {
        setShowShoppingCart(!showShoppingCart);
    }

    const handleExit = () => {
        cleanCustomerSessionAction();
    }

    const handleShowProfileMenu = () => {
        setShowProfileMenu(!showProfileMenu);
    }

    const handleOrder = () => {
        if(ShoppingCartReducer.products.length > 0){
            if(localStorage.getItem("sessionId")){
                navigate('/checkout')
            } else {
                handleShowShoppingCart();
                handleShowLoginModal();
            }
        }
    }
    

    return(
        <HomeLayoutView 
            categories={HomeDataReducer.menuCategories}
            bottomBarStatus={bottomBarStatus}
            onHomeClick={handleHomeClick}
            onCategoriesClick={handleCategoriesClick}
            onProfileClick={handleProfileClick}
            searchWord={searchWord}
            onSearchChange={handleSearchChange}
            onSearchClick={handleSearchClick}
            searchAutocomplete={searchAutocomplete}
            onSearchAutocompleteClean={handleSearchAutocompleteClean}
            ShoppingCartReducer={ShoppingCartReducer}
            showShoppingCart={showShoppingCart}
            onShowShoppingCart={handleShowShoppingCart}
            showSignUpModal={showSignUpModal}
            onShowSignUpModal={handleShowSignUpModal}
            showLoginModal={showLoginModal}
            onShowLoginModal={handleShowLoginModal}
            customerSession={CustomerSessionReducer.customerSessionData}
            onExit={handleExit}
            showProfileMenu={showProfileMenu}
            onShowProfileMenu={handleShowProfileMenu}
            onOrder={handleOrder}
        />
    );
}

const mapStateToProps = ({ ShoppingCartReducer, HomeDataReducer, CustomerSessionReducer }) => {
    return {
        ShoppingCartReducer,
        HomeDataReducer,
        CustomerSessionReducer
    };
};

const mapDispatchToProps = {
    clearProductsAction,
    cleanCustomerSessionAction,
    getHomeDataAction
}


export default connect(mapStateToProps, mapDispatchToProps)(HomeLayout);