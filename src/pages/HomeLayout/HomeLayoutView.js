import Home from '../Home';
import Product from '../Product';
import Category from '../Category';
import SubCategory from '../SubCategory';
import Search from '../Search';
import { Link, Route, Routes } from 'react-router-dom';
import { MenuOutlined, UserOutlined, ShoppingCartOutlined, UnorderedListOutlined, OrderedListOutlined, HomeOutlined, SearchOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router';
import Logo from '../../img/logo.png';
import MenuItem from '../../components/MenuItem';
import ShoppingCartList from '../../components/ShoppingCartList';
import SignUpModal from '../../components/SignUpModal';
import LoginModal from '../../components/LoginModal';
import CheckoutModal from '../../components/CheckoutModal';
import DefaultImg from '../../img/default.png';
import './HomeLayout.css';
import CustomLink from '../../components/CustomLink';



const Header = (props) => {

    const { searchWord, onSearchChange, onSearchClick, searchAutocomplete, onSearchAutocompleteClean, ShoppingCartReducer, onShowShoppingCart, onShowSignUpModal, onShowLoginModal, customerSession, onExit, showProfileMenu, onShowProfileMenu } = props;

    return(
        <div className="home-header">
            <div className="home-header-top">
                {/*<div className="home-header-menu home-header-icon"><MenuOutlined /></div>*/}
                <Link to={{ pathname: "/" }}>
                    <div className="home-header-logo"><img className="home-header-logo-img" src={ Logo } /></div>
                </Link>
                <div className="home-header-desktop-searchbar">
                    <div className="home-header-desktop-searchbar-container">
                        <input className="home-header-desktop-searchbar-txt" type="text" placeholder="Busca tus productos" value={searchWord} onChange={onSearchChange} />
                        <button className="home-header-desktop-searchbar-button" onClick={onSearchClick}><SearchOutlined /></button>
                    </div>
                    { searchAutocomplete.products.length > 0 &&
                    <div className="home-header-desktop-searchbar-autocomplete">
                        {searchAutocomplete.products.map(
                            (product, index) => {
                                return(
                                    <AutocompleteItem key={index} item={product} onSearchAutocompleteClean={onSearchAutocompleteClean} />
                                );
                            }
                        )}
                    </div>
                    }
                </div>
                { customerSession.customer ?
                    <div>
                        <CustomLink onClick={onShowProfileMenu}>
                            <div className="home-header-customer-profile">
                                <div className="home-header-customer-profile-icon-container"><UserOutlined /></div>
                                <div className="home-header-customer-profile-label">{customerSession.customer.name}</div>
                            </div>
                        </CustomLink>
                        <div className={showProfileMenu ? "home-header-customer-menu-active" : "home-header-customer-menu" }>
                            <CustomLink onClick={onExit}><div className="home-header-customer-menu-option">Salir</div></CustomLink>
                        </div>
                    </div>
                    :
                    <div className="home-header-desktop-links">
                        <button className="home-header-desktop-link" onClick={onShowSignUpModal}>Registrate</button>
                        <div>o</div>
                        <button className="home-header-desktop-link" onClick={onShowLoginModal}>Inicia sesión</button>
                    </div>
                }
                <div className="home-header-right-icons">
                    <button className="home-header-shopping-cart home-header-icon" onClick={onShowShoppingCart}><ShoppingCartOutlined /></button>
                    { ShoppingCartReducer.products.length > 0 && <div className="home-header-shopping-cart-qty">{ ShoppingCartReducer.products.length }</div> }
                </div>
            </div>
            <div className="home-header-searchbar">
                <div className="home-header-searchbar-container">
                    <input className="home-header-searchbar-txt" type="text" placeholder="Busca tus productos" value={searchWord} onChange={onSearchChange} />
                    <button className="home-header-searchbar-button" onClick={onSearchClick}><SearchOutlined /></button>
                </div>
                { searchAutocomplete.products.length > 0 &&
                    <div className="home-header-searchbar-autocomplete">
                        {searchAutocomplete.products.map(
                            (product, index) => {
                                return(
                                    <AutocompleteItem key={index} item={product} onSearchAutocompleteClean={onSearchAutocompleteClean} />
                                );
                            }
                        )}
                    </div>
                }
            </div>
        </div>
    );
}

const AutocompleteItem = (props) => {

    const { item, onSearchAutocompleteClean } = props;

    const navigate = useNavigate();

    const handleImgError = (e) => {
        e.currentTarget.src = DefaultImg;
        e.currentTarget.onError = null;
    }

    const handleClick = () => {
        onSearchAutocompleteClean();
        navigate('/product/' + item.id + '/' + item.price.um);
    }

    return(
        <div className="home-header-desktop-searchbar-autocomplete-item">
            <button onClick={handleClick} className="home-header-desktop-searchbar-autocomplete-link">
                <div className="home-header-desktop-searchbar-autocomplete-item-img-container"><img className="home-header-desktop-searchbar-autocomplete-item-img" src={item.imgUrl} onError={handleImgError} /></div>
                <div className="home-header-desktop-searchbar-autocomplete-item-name">{item.name}</div>
            </button>
        </div>
    );
}

const Footer = (props) => {

    const { bottomBarStatus, onCategoriesClick, onHomeClick, onProfileClick } = props;

    return(
        <div className="home-footer">
            <div className={ bottomBarStatus.showHome ? "icon-footer-selected" : "icon-footer"} onClick={onHomeClick}>
                <HomeOutlined />
                <div className="home-footer-icon-label">Inicio</div>
            </div>
            <div className={ bottomBarStatus.showMenu ? "icon-footer-selected" : "icon-footer"} onClick={onCategoriesClick}>
                <UnorderedListOutlined />
                <div className="home-footer-icon-label">Categorias</div>
            </div>
            <div className={ bottomBarStatus.showProfile ? "icon-footer-selected" : "icon-footer"} onClick={onProfileClick}>
                <UserOutlined />
                <div className="home-footer-icon-label">Perfil</div>
            </div>
            
        </div>
    );
}

const CategoriesMenu = (props) => {

    const { categories, bottomBarStatus } = props;

    return (
        <div className={ bottomBarStatus.showMenu ? "home-menu-container-active" : "home-menu-container"}>
            <div className="home-menu-title">Categorias</div>
            <div className="home-menu-items-container">
                { categories.map(
                    (category, index) => {
                        return ( <MenuItem key={index} item={category} />);
                    }
                ) }
            </div>
        </div>
    );
}

const HomeLayoutView = (props) => {
    
    const { bottomBarStatus, onHomeClick, onCategoriesClick, onProfileClick, categories, searchWord, onSearchChange, onSearchClick, searchAutocomplete, onSearchAutocompleteClean, ShoppingCartReducer, showShoppingCart, onShowShoppingCart, showSignUpModal, onShowSignUpModal, showLoginModal, onShowLoginModal, customerSession, onExit, showProfileMenu, onShowProfileMenu, onOrder } = props;

    return(
        <div className="home-main-container">
            <Header
                searchWord={searchWord}
                onSearchChange={onSearchChange}
                onSearchClick={onSearchClick}
                searchAutocomplete={searchAutocomplete}
                onSearchAutocompleteClean={onSearchAutocompleteClean}
                ShoppingCartReducer={ShoppingCartReducer}
                onShowShoppingCart={onShowShoppingCart}
                onShowSignUpModal={onShowSignUpModal}
                onShowLoginModal={onShowLoginModal}
                customerSession={customerSession}
                onExit={onExit}
                showProfileMenu={showProfileMenu}
                onShowProfileMenu={onShowProfileMenu}
            />
            <ShoppingCartList
                products={ShoppingCartReducer.products}
                showShoppingCart={showShoppingCart}
                onOrder={onOrder}
            />
            <div className="home-main-wrap">
                <CategoriesMenu 
                    categories={categories}
                    bottomBarStatus={bottomBarStatus}

                />
                <div className="home-main-view" >
                    <Routes>
                        <Route path="product/:productId/:mu" element={<Product />} />
                        <Route path="category/:categoryId" element={<Category />} />
                        <Route path="subcategory/:subCategoryId" element={<SubCategory />} />
                        <Route path="search/:word" element={<Search />} />
                        <Route path="/" element={<Home />} />
                    </Routes>    
                </div>
            </div>
            <Footer 
                bottomBarStatus={bottomBarStatus} 
                onHomeClick={onHomeClick}
                onCategoriesClick={onCategoriesClick}
                onProfileClick={onProfileClick}
            />
            <SignUpModal showModal={showSignUpModal} onShowSignUpModal={onShowSignUpModal} />
            <LoginModal showModal={showLoginModal} onShowLoginModal={onShowLoginModal} onShowSignupModal={onShowSignUpModal} />
        </div>
    );
}

export default HomeLayoutView;