export const setProductsAction = (product) => (dispatch, getState) => {

    const { products } = getState().ShoppingCartReducer;

    let newProducts = [ ...products ];

    const index = newProducts.findIndex( p => (p.id === product.id) && (p.price.um === product.price.um) );

    if (index > -1) {
        if(product.qty > 0){
            newProducts[index].qty = product.qty
        } else {
            newProducts.splice(index, 1);
        }
    } else {
        if(product.qty > 0){
            newProducts.push(product);
        }
    }

    let total = 0;
    for(product of newProducts) {
        total = total + (product.qty * product.price.price);
    }

    dispatch({
        type: "SET_PRODUCTS",
        payload: newProducts
    });

    dispatch({
        type: "SET_TOTAL",
        payload: total
    });
}

export const clearProductsAction = () => (dispatch) => {
    dispatch({
        type: "CLEAN_PRODUCTS"
    });

    dispatch({
        type: "SET_TOTAL",
        payload: 0
    });
}