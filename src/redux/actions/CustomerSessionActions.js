import axios from 'axios';
import { httpGet } from '../../http';

export const setCustomerSessionLoadingAction = () => (dispatch) => {
    dispatch({
        type: "SET_LOADING_ON"
    });
}

export const getCustomerSessionAction = (sessionId) => async (dispatch) => {
    dispatch({
        type: "SET_LOADING_ON"
    });

    try{
        const sessionResp = await httpGet('/customer/session/' + sessionId);
        console.log("GET SESSION");
        console.log(sessionResp);

        dispatch({
            type: "SET_CUSTOMER_SESSION_DATA",
            payload: sessionResp.data
        });
    } catch (error) {
        console.log(error);
        if (error instanceof axios.AxiosError){
            if((error.response.data.code) && (error.response.data.code !== 'ERROR_DATA_NOT_FOUND')){
                localStorage.removeItem("sessionId");
                dispatch({
                    type: "CLEAN_CUSTOMER_SESSION_DATA"
                });
            } else {
                dispatch({
                    type: "SET_ERROR",
                    payload: { onError: true, msg: 'Un error ha ocurrido al recuperar la sesión.' }
                });
            }
        } else {
            dispatch({
                type: "SET_ERROR",
                payload: { onError: true, msg: 'Un error ha ocurrido al recuperar la sesión.' }
            });
        }
    }

    dispatch({
        type: "SET_LOADING_OFF"
    });
}

export const cleanCustomerSessionAction = () => async (dispatch) => {
    dispatch({
        type: "CLEAN_CUSTOMER_SESSION_DATA"
    });
    localStorage.removeItem("sessionId");
}