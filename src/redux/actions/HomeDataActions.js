import { httpGet } from '../../http';

export const setHomeDataLoadingAction = () => (dispatch) => {
    dispatch({
        type: "SET_LOADING_ON"
    });
}

export const getHomeDataAction = () => async (dispatch) => {

    dispatch({
        type: "SET_LOADING_ON"
    });

    try{
        const menuCategoriesResp = await httpGet('/category/l1');
        const productCategoriesResp = await httpGet("/product/categories");

        console.log(menuCategoriesResp);

        console.log(productCategoriesResp.data);

        dispatch({
            type: "SET_MENU_CATEGORIES",
            payload: menuCategoriesResp.data
        });
        
        dispatch({
            type: "SET_PRODUCT_CATEGORIES",
            payload: productCategoriesResp.data
        });
    } catch (error) {
        console.log(error);
    }

    dispatch({
        type: "SET_LOADING_OFF"
    });
}