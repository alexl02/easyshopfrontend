const INITIAL_STATE = {
    products: [],
    total: 0
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case "SET_PRODUCTS":
            return {...state, products: action.payload};
        case "CLEAN_PRODUCTS":
            return {...state, products: []};
        case "SET_TOTAL":
            return {...state, total: action.payload};
                                
            default: return state;
    }
}