const INITIAL_STATE = {
    loading: true,
    productCategories: [],
    menuCategories: [],
    error: {}
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case "SET_LOADING_ON":
            return {...state, loading: true}
        case "SET_LOADING_OFF":
            return {...state, loading: false}
        case "SET_PRODUCT_CATEGORIES":
            return {...state, productCategories: action.payload}
        case "SET_MENU_CATEGORIES":
            return {...state, menuCategories: action.payload}
        case "SET_ERROR":
            return {...state, loading: false, error: action.payload}
                                
            default: return state;
    }
}