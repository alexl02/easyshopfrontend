const INITIAL_STATE = {
    loading: true,
    customerSessionData: {},
    error: {}
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case "SET_LOADING_ON":
            return {...state, loading: true}
        case "SET_LOADING_OFF":
            return {...state, loading: false}
        case "SET_CUSTOMER_SESSION_DATA":
            return {...state, customerSessionData: action.payload}
        case "CLEAN_CUSTOMER_SESSION_DATA":
            return {...state, customerSessionData: {}}
        case "SET_ERROR":
            return {...state, loading: false, error: action.payload}
                                
            default: return state;
    }
}