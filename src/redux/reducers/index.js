import { combineReducers } from 'redux';
import ShoppingCartReducer from './ShoppingCartReducer';
import HomeDataReducer from './HomeDataReducer';
import CustomerSessionReducer from './CustomerSessionReducer';

export default combineReducers({
    ShoppingCartReducer,
    HomeDataReducer,
    CustomerSessionReducer
});