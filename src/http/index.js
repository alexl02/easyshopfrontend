import axios from 'axios';

//const BE_URL = 'https://5eca-186-168-125-218.ngrok.io/easyshop/api/v1';
const BE_URL = 'http://18.237.91.254:9090/easyshop/api/v1';
const apiKey = "cec3c6a996891875a89756a19680a7a5";

export const httpGet = async (path) => {
   
    let options = {
        headers: { "Content-Type": "application/json", "api_key": apiKey }
    };
    
    let resp = await axios.get(BE_URL + path, options);

    return resp;
}

export const httpPost = async (path, data) => {

    let options = {
        headers: { "Content-Type": "application/json", "api_key": apiKey }
    };
    
    let resp = await axios.post(BE_URL + path, data, options);
    return resp
}

export const httpPut = async (path, data) => {

    let options = {
        headers: { "Content-Type": "application/json", "api_key": apiKey }
    };
    
    let resp = await axios.put(BE_URL + path, data, options);
    return resp
}

export const httpDelete = async (path) => {

    let options = {
        headers: { "Content-Type": "application/json", "api_key": apiKey }
    };
    
    let resp = await axios.delete(BE_URL + path, options);
    return resp
}